const { Nuxt, Builder } = require('nuxt')
const app = require('express')()
const cors = require('cors')()
const port = process.env.PORT || 4001

// We instantiate Nuxt.js with the options
let config = require('./nuxt.config.js')
const nuxt = new Nuxt(config)

app.use(cors)
app.use(nuxt.render)

// Build only in dev mode
if (config.dev) {
  new Builder(nuxt).build()
  .catch((error) => {
    console.error(error)
    process.exit(1)
  })
}

// // Listen the server
app.listen(port, '0.0.0.0')
