import io from 'socket.io-client'
import Cookie from 'js-cookie'

class Socket {
  constructor(serverUrl, store, heartbeatTime = 15000) {
    if (Socket.instance) {
      return Socket.instance
    }

    Socket.instance = this

    this.heartbeatTime = heartbeatTime
    this.store = store
    this.serverUrl = serverUrl

    return Socket.instance
  }

  connect() {
    this.socket = io.connect(this.serverUrl, {
      transportOptions: ['websocket'],
    })

    const self = this

    this.socket.on('connect', () => self.startHeartbeat())
    this.socket.on('diconnect', () => self.stopHeartbeat())
    this.socket.on('newMessage', data => self.newMessage(data))

    this.join_chats()
  }

  authenticated(eventName, data) {
    data = { ...data, token: Cookie.get('token') }
    this.socket.emit(eventName, data)
  }

  join_chats() {
    this.authenticated('join_chats')
  }

  newMessage(data) {
    this.store.dispatch('chat/newMessage', data)
  }

  beat() {
    this.authenticated('heartbeat')
  }

  startHeartbeat() {
    const self = this
    this.heartbeater = setInterval(() => self.beat(), this.heartbeatTime)
  }

  stopHeartbeat() {
    clearInterval(this.heartbeater)
  }
}

export default Socket
