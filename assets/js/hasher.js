import jsSha from 'jssha'

class Hasher {
  constructor() {
    this.hasher = new jsSha('SHA-512', 'TEXT')
  }

  hash(input) {
    this.hasher.update(input)

    const res = this.hasher.getHash('HEX')
    this.hasher = new jsSha('SHA-512', 'TEXT')

    return res
  }
}

export default new Hasher()
