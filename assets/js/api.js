import Cookie from 'js-cookie'
import axios from '~/plugins/axios'

class Api {
  authenticated(method, url, data = {}) {
    const headers = {
      Authorization: `Bearer ${Cookie.get('token')}`,
    }

    return axios({ method, url, data, headers })
  }

  login(username, passwordHash) {
    return axios.post('/auth/login', {
      username,
      passwordHash,
    })
  }

  register(username, passwordHash) {
    return axios.post('/auth/register', {
      username,
      passwordHash,
    })
  }

  loadChats() {
    return this.authenticated('get', '/chat')
  }

  loadChat(chatId) {
    return this.authenticated('get', `/chat/${chatId}`)
  }

  createChat(userId) {
    return this.authenticated('post', '/chat', { userId })
  }

  sendMessage(messageBody, chatId) {
    const data = { messageBody }
    return this.authenticated('put', `/chat/${chatId}`, data)
  }

  searchUser(query) {
    const data = { query: query }
    return this.authenticated('post', '/user/search', data)
  }
}

export default new Api()
