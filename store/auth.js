import Cookie from 'js-cookie'

export const state = () => ({
  token: Cookie.get('token') || null,
})

export const mutations = {
  setToken(state, token) {
    state.token = token
  },
}

export const actions = {
  updateToken({ commit }, token) {
    commit('setToken', token)
    Cookie.set('token', token, { expires: 7 })
  },

  invalidateToken({ commit }) {
    commit('setToken', null)
    Cookie.remove('token')
  },
}
