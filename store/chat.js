import Api from '~/assets/js/api'
import Vue from 'vue'

export const state = () => ({
  chatList: [],
  currentChat: null,
})

export const mutations = {
  setChatList(state, chatList) {
    state.chatList = chatList
  },

  setChat(state, chat) {
    const idx = state.chatList.findIndex(({ id }) => id == chat.id)

    if (idx >= 0) {
      Vue.set(state.chatList, idx, chat)
    }
  },

  selectChat(state, chatId) {
    const idx = state.chatList.findIndex(({ id }) => id == chatId)

    if (idx >= 0) {
      state.currentChat = idx
    }

    Vue.set(state.chatList[idx], 'hasUnreadMessages', false)
  },

  addChat(state, chat) {
    state.chatList.push(chat)
  },

  addMessage({ currentChat, chatList }, { message, username }) {
    chatList[currentChat].messages.push({
      message_body: message,
      created_at: Date.now(),
      username: username,
    })

    // Ensure preview is updated back in chatList
    Vue.set(chatList[currentChat], 'preview', message)
  },

  notifyMessage(state, { message, chatId }) {
    const { chatList } = state
    const idx = state.chatList.findIndex(({ id }) => id == chatId)

    Vue.set(chatList[idx], 'preview', message)
    Vue.set(chatList[idx], 'hasUnreadMessages', true)
  },
}

export const actions = {
  loadChats({ commit }) {
    Api.loadChats().then(({ data }) => commit('setChatList', data.chats))
  },

  loadChat({ commit }, chatId) {
    Api.loadChat(chatId).then(({ data }) => {
      commit('setChat', data)
      commit('selectChat', data.id)
    })
  },

  createChat({ commit }, userId) {
    Api.createChat(userId).then(({ data }) => {
      commit('addChat', data)
      commit('selectChat', data.id)
    })
  },

  newMessage(store, msg) {
    const {
      getters: { selectedChat },
      commit,
    } = store

    if (msg.chatId == selectedChat.id) {
      commit('addMessage', msg)
    } else {
      commit('notifyMessage', msg)
    }
  },
}

export const getters = {
  selectedChat: ({ currentChat, chatList }) => chatList[currentChat] || {},
}
