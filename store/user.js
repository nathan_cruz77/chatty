export const state = () => ({
  id: null,
  username: null,
  email: null,
  createdAt: null,
})

export const mutations = {
  setUser(state, { user }) {
    state.id = user.id
    state.username = user.username
    state.email = user.email
    state.createdAt = user.created_at
  },

  setUsername(state, username) {
    state.username = username
  },
}

export const getters = {
  username: ({ username }) => username,
}
