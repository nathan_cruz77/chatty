export const state = () => ({
  show: false,
})

export const mutations = {
  setSearch(state, value) {
    state.search = value
  },
}

export const actions = {
  toggleSearch({ commit, state }) {
    commit('setSearch', !state.search)
  },

  enableSearch({ commit }) {
    commit('setSearch', true)
  },

  disableSearch({ commit }) {
    commit('setSearch', false)
  },
}

export const getters = {
  search: ({ search }) => search,
}
