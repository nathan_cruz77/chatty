let routerBase = {}

if (process.env.DEPLOY_ENV === 'GH_PAGES') {
  routerBase = { router: { base: '/chatty/' } }
}

module.exports = {
  ...routerBase,

  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:5000',
  },

  css: [
    '~/assets/sass/main.sass',
  ],

  modules: [
   '@nuxtjs/axios',
  ],

  head: {
    meta: [{ charset: 'utf-8' }],
    title: 'Chatty - Chat application built with Nuxt',
  },

  dev: (process.env.NODE_ENV !== 'production'),

  build: {
    extend (config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: "pre",
          test: /\.(js|vue)$/,
          loader: "eslint-loader",
          exclude: /(node_modules)/
        })
      }

      // Load svg with 'vue-svg-loader'
      const urlLoader = config.module.rules.find(({ loader }) => loader === 'url-loader')
      urlLoader.test = /\.(png|jpe?g|gif)$/

      config.module.rules.push({
        test: /\.svg$/,
        loader: 'vue-svg-loader',
        exclude: /node_modules/
      })
    },
  },

  mode: 'spa',
}
