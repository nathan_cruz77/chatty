import Socket from '~/assets/js/socket'

export default ({ store, env }) => {
  const socket = new Socket(env.baseUrl, store)
  socket.connect()
}
